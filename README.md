![](logo_ioasys.png =200x)

# Desafio React Native - ioasys - Thales Kenne

Investor Haven is an app that consumes an API and lists Startups. The user can also search startups by name.

### :cd: Technologies used

- React-Native
- Redux and Redux-persist
- Functional components
- React-Navigation

### :sunglasses: Reasoning behind features

- I decided to create a brand since the name "Empresas" didn't fit quite well with the app being built in english. That also gave me some room to style my app more comfortably.

- Searched enterprises are saved under a different key in the reducer. This decreases the amount of calls to `/enterprises`, since the rendering of startups doesnt overwrite the full list of enterprises.

- I persisted the authentication state to make sure the user doesn't need to login every time they open the app.

- I did NOT use the route `/enterprises/{id}` for two reasons:
<ul>
	<ul>
		<li>I'd need to create another screen to list details and that would only mean extra work to showcase abilities I had already proven.
		</li>
		<li>All the data for each enterprise was already loaded from the index route. Calling the function to render details would be unnecessary use of resources, just pass the data as props to the DetailScreen.
		</li>
	</ul>
</ul>

- I didn't have time to finish the search bar to include the `type` search. The service is prepared to handle a type request. I would have looped through the enterprises to filter unique types, set those in a reducer and add those as options in a dropdown in the search bar. Optimally the types would come from a separate endpoint, but I'd need to work with what is available, thus the looping.

- I built an android adaptive icon and used a custom font just to brand the app a little better.

- Unfortunately I don't own a Mac, so I could only test/configure the app for Android.

### :book: Why each library was used

For redux:

- react-redux
- redux
- redux-persist `To persist the reducer across sessions`
- redux-thunk `To make async actions`

For navigation: (most are required by react-navigation)

- @react-navigation/native
- @react-navigation/stack
- react-native-gesture-handler
- react-native-reanimated
- react-native-safe-area-context
- react-native-screens
- @react-native-community/masked-view

To persist auth state:

- @react-native-community/async-storage `Required to persist with redux-persist`

To make api calls:

- axios

To render icons:

- react-native-vector-icons

From React-Native init:

- react
- react-native
- devDependencies: {
  - @babel/core
  - @babel/runtime
  - @react-native-community/eslint-config
  - babel-jest
  - eslint
  - jest
  - metro-react-native-babel-preset
  - react-test-renderer </br>}

### :exclamation: Known issues

- Redux-persist is sometimes persisting everything, despite only whitelisting the auth reducer. I didn't have time to investigate. This causes the app to not list the enterprises.

### :computer: Instructions to run

- Make sure you have an emulator or your android phone has USB debugging enabled
- Clone the repo
- Run `yarn` to install dependencies
- Run `yarn android` to install the app on the phone. If your server doesn't start automatically after installation is complete, run `yarn start` and reload the application.

### :camera: Preview

![](/src/assets/preview1.jpeg =200x)
![](/src/assets/preview2.jpeg =200x)
