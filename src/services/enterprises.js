import api from "./api";

export const fetchEnterprises = async ({ headers }) => {
  try {
    const response = await api.get("/enterprises", { headers });
    return response.data;
  } catch (err) {
    return err.response.data;
  }
};

export const searchEnterprises = async ({ name, type, headers }) => {
  let queryString = "";
  if (name) {
    queryString = queryString.concat(name && `name=${name}`);
  }
  if (name && type) {
    queryString = queryString.concat(name && "&");
  }
  if (type) {
    queryString = queryString.concat(type && `enterprise_types=${type}`);
  }

  try {
    const response = await api.get("/enterprises?" + queryString, { headers });
    return response.data;
  } catch (err) {
    return err.response.data;
  }
};
