import api from "./api";

export const SignIn = async (credentials) => {
  try {
    const response = await api.post("/users/auth/sign_in", credentials);
    const { "access-token": accessToken, client, expiry, uid } = response.headers;
    return { ...response.data, expiry, headers: { "access-token": accessToken, client, uid } };
  } catch (err) {
    return err.response.data;
  }
};
