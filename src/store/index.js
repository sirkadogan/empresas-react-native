import { createStore, applyMiddleware } from "redux";
import thunk from "redux-thunk";

import AsyncStorage from "@react-native-community/async-storage";

import { persistStore, persistReducer } from "redux-persist";

import rootReducer from "./reducers";

const persistConfig = {
  key: "auth",
  storage: AsyncStorage,
  whitelist: ["auth"], // which reducer want to store
};
const pReducer = persistReducer(persistConfig, rootReducer);

const store = createStore(pReducer, applyMiddleware(thunk));
const persistor = persistStore(store);

export { persistor, store };
