import ACTION_TYPES from "../actions/types";

const INITIAL_STATE = {
  errors: [],
  enterprises: [],
  searchedEnterprises: [],
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case ACTION_TYPES.GET_ENTERPRISES:
      return { ...state, ...action.payload };
    case ACTION_TYPES.SEARCH_ENTERPRISES:
      return { ...state, searchedEnterprises: [...action.payload] };
    default:
      return state;
  }
};
