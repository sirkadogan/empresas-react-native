import { combineReducers } from "redux";

import authReducer from "./auth";
import enterprisesReducer from "./enterprises";

export default combineReducers({
  auth: authReducer,
  enterprises: enterprisesReducer,
});
