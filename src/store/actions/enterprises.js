import ACTION_TYPES from "./types";

// Services
import { fetchEnterprises, searchEnterprises } from "../../services/enterprises";

export const getEnterprises = (credentials) => {
  return async (dispatch, getState) => {
    const { headers } = getState().auth;
    // Make request

    const response = await fetchEnterprises({ headers });
    return dispatch({
      type: ACTION_TYPES.GET_ENTERPRISES,
      payload: response,
    });
  };
};

export const search = (params) => {
  return async (dispatch, getState) => {
    const { headers } = getState().auth;
    dispatch(clearSearch());
    const response = await searchEnterprises({ ...params, headers });
    console.log(response);

    dispatch({
      type: ACTION_TYPES.SEARCH_ENTERPRISES,
      payload: response.enterprises,
    });

    return response.enterprises;
  };
};

export const clearSearch = () => {
  return {
    type: ACTION_TYPES.SEARCH_ENTERPRISES,
    payload: [],
  };
};
