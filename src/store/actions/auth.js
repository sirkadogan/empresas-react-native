import ACTION_TYPES from "./types";

// Services
import { SignIn } from "../../services/login";

export const authenticate = (credentials) => {
  return async (dispatch) => {
    // Clears auth state
    dispatch({
      type: ACTION_TYPES.AUTHENTICATE,
      payload: {},
    });

    // Make request
    const response = await SignIn(credentials);
    console.log(response);
    return dispatch({
      type: ACTION_TYPES.AUTHENTICATE,
      payload: response,
    });
  };
};

export const logout = () => {
  return {
    type: ACTION_TYPES.AUTHENTICATE,
    payload: {},
  };
};
