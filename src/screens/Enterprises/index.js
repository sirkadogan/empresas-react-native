import React, { useCallback, useEffect, useState } from "react";

import { View, Text, ActivityIndicator, FlatList } from "react-native";

// Components
import CompanyCard from "../../components/CompanyCard";
import SearchBar from "../../components/SearchBar";

// Redux
import { connect } from "react-redux";
import { getEnterprises } from "../../store/actions/enterprises";

// Constants
import COLORS from "../../constants/colors.json";

import styles from "./styles";

const EnterprisesScreen = (props) => {
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    async function init() {
      await props.getEnterprises();
      setLoading(false);
    }
    init();
  }, []);

  const shouldRenderError = useCallback(() => {
    if (props.errors.length > 0) return true;
    return false;
  }, [props.errors]);

  const renderSearchBar = useCallback(() => {
    return <SearchBar setLoading={setLoading} />;
  }, []);

  const ListEmptyComponent = useCallback(() => {
    if (loading) return <View style={styles.emptyContainer}>{loading && <ActivityIndicator color={COLORS.primary} size="large" />}</View>;
    if (shouldRenderError())
      return props.errors.map((error) => (
        <View style={styles.emptyContainer}>
          <Text style={styles.emptyText} key={error}>
            {error}
          </Text>
        </View>
      ));

    return (
      <View style={styles.emptyContainer}>
        <Text style={styles.emptyText}>Couldn't find any enterprise</Text>
      </View>
    );
  }, [shouldRenderError, loading]);

  const renderData = useCallback(() => {
    if (loading) return [];

    return props.searchedEnterprises.length > 0 ? props.searchedEnterprises : props.enterprises;
  }, [props, loading]);

  return (
    <View style={{ flex: 1, backgroundColor: "#41444b", justifyContent: "flex-start" }}>
      <FlatList
        ListHeaderComponent={renderSearchBar}
        ListEmptyComponent={ListEmptyComponent}
        keyExtractor={(item) => item.id.toString()}
        data={renderData()}
        renderItem={({ item }) => <CompanyCard {...item} />}
      />
    </View>
  );
};

const mapStateToProps = (state) => {
  return {
    enterprises: state.enterprises.enterprises,
    searchedEnterprises: state.enterprises.searchedEnterprises,
    errors: state.enterprises.errors,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getEnterprises: () => dispatch(getEnterprises()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(EnterprisesScreen);
