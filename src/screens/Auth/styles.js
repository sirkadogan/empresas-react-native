import { StyleSheet } from "react-native";
import COLORS from "../../constants/colors.json";

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#41444b",
    justifyContent: "center",
    alignItems: "center",
  },
  body: {
    flex: 1,
    width: "100%",
    justifyContent: "center",
  },
  logoContainer: {
    padding: 30,
    alignItems: "center",
  },
  logo: {
    height: 130,
  },
  smallLogoContainer: {
    padding: 10,
    alignItems: "center",
  },
  smallLogo: {
    height: 130,
  },
  poweredBy: {
    width: 150,
  },
});
