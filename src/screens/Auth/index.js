import React, { useState, useCallback, useEffect } from "react";

import { View, Image, Keyboard } from "react-native";

// Components
import Input from "../../components/Input";
import Button from "../../components/Button";
import LoginError from "../../components/LoginError";

// Redux
import { connect } from "react-redux";
import { authenticate } from "../../store/actions/auth";

// Assets
import logo from "../../assets/logo.png";
import poweredBy from "../../assets/poweredBy.png";
import styles from "./styles";

const AuthScreen = (props) => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [loading, setLoading] = useState(false);
  const [isKeyboardOpen, setIsKeyboardOpen] = useState(false);

  useEffect(() => {
    Keyboard.addListener("keyboardDidShow", () => setIsKeyboardOpen(true));
    Keyboard.addListener("keyboardDidHide", () => setIsKeyboardOpen(false));

    return () => {
      Keyboard.removeAllListeners("keyboardDidShow");
      Keyboard.removeAllListeners("keyboardDidHide");
    };
  }, []);

  const login = useCallback(async () => {
    Keyboard.dismiss();
    setLoading(true);
    await props.authenticate({ email, password });
    setLoading(false);
  }, [email, password]);

  return (
    <View style={styles.container}>
      <View style={styles.body}>
        <View style={isKeyboardOpen ? styles.smallLogoContainer : styles.logoContainer}>
          <Image source={logo} style={isKeyboardOpen ? styles.smallLogo : styles.logo} resizeMode="contain" />
        </View>
        <Input value={email} onChange={setEmail} label="Email" keyboardType="email-address" />
        <Input value={password} onChange={setPassword} label="Password" secureTextEntry={true} />
        {props.loginSuccess === false && props.errorMessages.map((message) => <LoginError key={message}>{message}</LoginError>)}

        <Button title="LOGIN" loading={loading} onPress={login} />
      </View>
      {!isKeyboardOpen && <Image source={poweredBy} style={styles.poweredBy} resizeMode="contain" />}
    </View>
  );
};

const mapStateToProps = (state) => {
  return {
    loginSuccess: state.auth.success,
    errorMessages: state.auth.errors,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    authenticate: (credentials) => dispatch(authenticate(credentials)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(AuthScreen);
