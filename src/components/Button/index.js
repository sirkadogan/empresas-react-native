import React from "react";

import { TouchableHighlight, Text, ActivityIndicator, View } from "react-native";

import COLORS from "../../constants/colors.json";

import styles from "./styles";

const Button = ({ onPress, title, loading }) => {
  return (
    <View style={styles.container}>
      <TouchableHighlight onPress={onPress}>
        <View style={styles.button}>
          {loading && <ActivityIndicator color={COLORS.dark} style={styles.loading} />}

          <Text style={styles.text}>{title}</Text>
        </View>
      </TouchableHighlight>
    </View>
  );
};

export default Button;
