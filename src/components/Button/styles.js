import { StyleSheet } from "react-native";
import COLORS from "../../constants/colors.json";

export default StyleSheet.create({
  container: {
    marginHorizontal: 15,
    marginTop: 15,
  },
  button: {
    backgroundColor: COLORS.primary,
    padding: 15,
    borderRadius: 5,
    justifyContent: "center",
    flexDirection: "row",
  },
  text: {
    color: COLORS.dark,
    fontWeight: "bold",
  },
  loading: {
    marginRight: 5,
  },
});
