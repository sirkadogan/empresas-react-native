import { StyleSheet } from "react-native";
import COLORS from "../../constants/colors.json";

export default StyleSheet.create({
  container: {
    marginHorizontal: 10,

    marginBottom: 5,
  },
  inputContainer: {
    flexDirection: "row",
  },
  input: {
    borderColor: COLORS.primary,
    borderWidth: 2,
    borderBottomLeftRadius: 5,
    borderTopLeftRadius: 5,
    backgroundColor: "#fff",
    width: "80%",
  },
  searchButton: {
    borderBottomRightRadius: 5,
    borderTopRightRadius: 5,
    backgroundColor: COLORS.primary,
    width: "20%",
    alignItems: "center",
    justifyContent: "center",
  },
  icon: {
    color: COLORS.dark,
  },
  clearSearchText: {
    color: COLORS.primary,
  },
  clearSearchContainer: {
    alignSelf: "flex-end",
    marginVertical: 10,
  },
});
