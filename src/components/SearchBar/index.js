import React, { useState, useCallback } from "react";
import { View, TextInput, TouchableHighlight, Text, ToastAndroid } from "react-native";

// Libraries
import Icon from "react-native-vector-icons/AntDesign";

// Redux
import { connect } from "react-redux";
import { search, clearSearch } from "../../store/actions/enterprises";

// Assets
import COLORS from "../../constants/colors.json";
import styles from "./styles";

const SearchBar = (props) => {
  const [value, setValue] = useState("");

  const handleSearch = useCallback(async () => {
    props.setLoading(true);
    const response = await props.search({ name: value });
    if (response.length < 1) {
      ToastAndroid.show("No enterprises found", ToastAndroid.LONG);
    }
    props.setLoading(false);
  }, [value, props.searchedEnterprises]);

  return (
    <View style={styles.container}>
      <View style={styles.inputContainer}>
        <TextInput
          value={value}
          onChangeText={(text) => setValue(text)}
          placeholder="Search enterprises"
          selectionColor={COLORS.dark}
          color={COLORS.dark}
          style={styles.input}
        />
        <TouchableHighlight onPress={handleSearch} style={styles.searchButton} disabled={!value}>
          <Icon name="search1" size={30} style={styles.icon} />
        </TouchableHighlight>
      </View>
      {props.searchedEnterprises.length > 0 && (
        <TouchableHighlight onPress={props.clearSearch} style={styles.clearSearchContainer}>
          <Text style={styles.clearSearchText}>Clear search</Text>
        </TouchableHighlight>
      )}
    </View>
  );
};

const mapStateToProps = (state) => {
  return {
    searchedEnterprises: state.enterprises.searchedEnterprises,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    search: (params) => dispatch(search(params)),
    clearSearch: () => dispatch(clearSearch()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(SearchBar);
