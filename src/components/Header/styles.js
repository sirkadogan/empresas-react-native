import { StyleSheet } from "react-native";
import COLORS from "../../constants/colors.json";

export default StyleSheet.create({
  container: {
    padding: 10,
    backgroundColor: COLORS.dark,
    flexDirection: "row",
    justifyContent: "space-between",
  },
  title: {
    fontSize: 30,
    marginLeft: 5,
    marginBottom: 5,
    color: COLORS.primary,
    // fontWeight: "bold",
    fontFamily: "Montserrat-Regular",
  },
  icon: {
    color: COLORS.primary,
    textAlignVertical: "center",
  },
});
