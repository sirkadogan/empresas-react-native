import React from "react";

import { Text, View, TouchableHighlight } from "react-native";

// Libraries
import Icon from "react-native-vector-icons/AntDesign";

// Redux
import { connect } from "react-redux";
import { logout } from "../../store/actions/auth";

import styles from "./styles";

const Header = (props) => {
  return (
    <View style={styles.container}>
      <Text style={styles.title}>Enterprises</Text>
      <TouchableHighlight
        onPress={() => {
          props.logoff();
        }}
        style={{ justifyContent: "center" }}
      >
        <Icon name="logout" size={26} style={styles.icon} />
      </TouchableHighlight>
    </View>
  );
};

const mapDispatchToProps = (dispatch) => {
  return {
    logoff: () => dispatch(logout()),
  };
};

export default connect(null, mapDispatchToProps)(Header);
