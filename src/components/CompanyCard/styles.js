import { StyleSheet } from "react-native";
import COLORS from "../../constants/colors.json";

export default StyleSheet.create({
  container: {
    borderWidth: 1,
    borderColor: COLORS.secondary,
    margin: 5,
    marginHorizontal: 10,
    borderRadius: 4,
    flexDirection: "row",
    justifyContent: "space-between",
  },
  infoContainer: {
    padding: 10,
    width: "70%",
  },
  title: {
    fontSize: 18,
    color: COLORS.primary,
    fontFamily: "Montserrat-Bold",
  },
  description: {
    fontSize: 8,
  },
  icon: {
    color: COLORS.primary,
    textAlignVertical: "center",
  },
  shareContainer: {
    backgroundColor: COLORS.secondary,
    width: "30%",
    alignItems: "center",
    padding: 5,
    justifyContent: "center",
  },
  shareLabel: {
    fontFamily: "Montserrat-Bold",
    fontSize: 8,
    alignSelf: "flex-start",
  },
  sharePrice: {
    fontSize: 25,
    color: COLORS.dark,
  },
  locationText: {
    color: COLORS.light,
    fontWeight: "bold",
    fontSize: 12,
  },
  infoBottom: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    marginTop: 5,
  },
  tag: {
    backgroundColor: COLORS.secondary,
    padding: 3,
    paddingHorizontal: 5,
    borderRadius: 5,
  },
  tagText: {
    fontWeight: "bold",
    fontSize: 13,
  },
});
