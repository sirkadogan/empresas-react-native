import React from "react";

import { Text, View, TouchableHighlight } from "react-native";

// Libraries
import Icon from "react-native-vector-icons/AntDesign";

import styles from "./styles";

const CompanyCard = (props) => {
  return (
    <View style={styles.container}>
      <View style={styles.infoContainer}>
        <Text style={styles.title}>{props.enterprise_name}</Text>
        <View style={styles.infoBottom}>
          <View style={styles.tag}>
            <Text style={styles.tagText}>{props.enterprise_type.enterprise_type_name.toUpperCase()}</Text>
          </View>
          <Text style={styles.locationText}>{props.city + " - " + props.country}</Text>
        </View>
      </View>

      <View style={styles.shareContainer}>
        <Text style={styles.shareLabel}>SHARE PRICE</Text>
        <Text style={styles.sharePrice}>${props.share_price}</Text>
      </View>
    </View>
  );
};

export default CompanyCard;
