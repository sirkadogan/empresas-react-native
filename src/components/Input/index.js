import React from "react";
import { View, Text, TextInput } from "react-native";
import COLORS from "../../constants/colors.json";
import styles from "./styles";

const Input = ({ onChange, label, value, ...rest }) => {
  return (
    <View style={styles.container}>
      <Text style={styles.label}>{label}</Text>
      <TextInput
        value={value}
        onChangeText={(text) => onChange(text)}
        selectionColor={COLORS.dark}
        color={COLORS.dark}
        style={styles.input}
        {...rest}
      />
    </View>
  );
};

export default Input;
