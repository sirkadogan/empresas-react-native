import { StyleSheet } from "react-native";
import COLORS from "../../constants/colors.json";

export default StyleSheet.create({
  container: {
    marginHorizontal: 15,
    margin: 5,
  },
  label: {
    marginLeft: 5,
    marginBottom: 5,
    color: COLORS.primary,
  },
  input: {
    borderColor: COLORS.primary,
    borderWidth: 2,
    borderRadius: 5,
    backgroundColor: "#fff",
  },
});
