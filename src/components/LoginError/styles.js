import { StyleSheet } from "react-native";
import COLORS from "../../constants/colors.json";

export default StyleSheet.create({
  container: {
    marginHorizontal: 15,
    alignItems: "flex-end",
  },
  text: {
    color: COLORS.primary,
  },
});
