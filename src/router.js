import React from "react";
import { createStackNavigator } from "@react-navigation/stack";

// Components
import Header from "./components/Header";

// Screens
import AuthScreen from "./screens/Auth";
import EnterprisesScreen from "./screens/Enterprises";

// Redux
import { connect } from "react-redux";

const Stack = createStackNavigator();

const Router = (props) => {
  return (
    <Stack.Navigator>
      {props.isLoggedIn ? (
        <>
          <Stack.Screen
            name="EnterprisesScreen"
            component={EnterprisesScreen}
            options={{
              header: () => <Header />,
            }}
          />
        </>
      ) : (
        <Stack.Screen
          name="AuthScreen"
          component={AuthScreen}
          options={{
            headerShown: false,
          }}
        />
      )}
    </Stack.Navigator>
  );
};

const mapStateToProps = (state) => {
  return {
    isLoggedIn: state.auth.success,
  };
};

export default connect(mapStateToProps)(Router);
